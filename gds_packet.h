//
// Created by erik.vonreis on 3/7/23.
//

#ifndef GDSBROADCASTTEST_GDS_PACKET_H
#define GDSBROADCASTTEST_GDS_PACKET_H

#include <netinet/in.h>

/// default IP port [7096]
const int 		frameXmitPort = 7096;
/// priority of daemon (lower values = higher priority) [1]
const int 		daemonPriority = 1;
/// default number of buffers for the sender [7]
const int		sndDefaultBuffers = 7;
/// packet size (in bytes) [64000]
const int		packetSize = 64000;
/// continues packet burst (in # of packets) [4]
const int		packetBurst = 4;
/// minimum time interval between bursts (in usec) [1000]
const int		packetBurstInterval = 1000;
/// sender delay tick if there are no packets to send (in usec) [1000]
const int		sndDelayTick = 1000;
/// receiver delay tick if there were no packets received (in usec) [1000]
const int		rcvDelayTick = 1000;
/// Maximum wait time for a lost packet in original stream (in usec) [10000]
const int            lostPacketWait = 15000; // used to be 10000
/// maximum number of packets allowed for retransmit [200]
const int		maximumRetransmit = 200;
/// sender socket input buffer length (in bytes) [65536]
const int 		sndInBuffersize = 65536;
/// sender socket output buffer length (in bytes) [262144]
const int 		sndOutBuffersize = 4 * 65536;
/// receiver socket input buffer length (in bytes) [262144]
const int 		rcvInBuffersize = 4 * 65536;
/// receiver socket input buffer maximum length (in bytes) [1048576]
const int 		rcvInBufferMax = 16 * 65536;
/// receiver socket output buffer length (in bytes) [65536]
const int 		rcvOutBuffersize = 65536;
/// receiver packet buffer length (in number of packets) [1024]
const int 		rcvpacketbuffersize = 1024;
/// maximum number of retries to rebroadcast [5]
const int 		maxRetry = 5;
/// timeout for each retry (in usec) [250000]
const int 		retryTimeout = 250000;
/// maximum value a sequnce can be out of sync [5]
const int		maxSequenceOutOfSync = 5;
/// number of data segments used for determining the average
/// retransmission rate [10.1]
const double		retransmissionAverage = 10.1;
/// artifically introduced receiver error rate [0], only valid if
/// compiled with DEBUG
const double		rcvErrorRate = 0;

/// broadcast packet type [123]
const int		PKT_BROADCAST = 123;
/// rebroadcast packet type [124]
const int		PKT_REBROADCAST = 124;
/// retransmit request packet [125]
const int		PKT_REQUEST_RETRANSMIT = 125;

/// Packet header (information is stored in network byteorder)
struct packetHeader {
  /// packet type
  int32_t		pktType;
  /// length of payload in bytes
  int32_t		pktLen;
  /// sequence number
  uint32_t		seq;
  /// packet number
  int32_t		pktNum;
  /// total number of packets in sequence
  int32_t		pktTotal;
  /// checksum of packet (currently not used)
  uint32_t		checksum;
  /// GPS time stamp of data
  uint32_t		timestamp;
  /// time length of data
  uint32_t		duration;

  void hton()
  {
    pktType = htonl (pktType);
    pktLen = htonl (pktLen);
    seq = htonl (seq);
    pktNum = htonl (pktNum);
    pktTotal = htonl (pktTotal);
    checksum = htonl (checksum);
    timestamp = htonl (timestamp);
    duration = htonl (duration);
  }

  void ntoh()
  {
    pktType = ntohl (pktType);
    pktLen = ntohl (pktLen);
    seq = ntohl (seq);
    pktNum = ntohl (pktNum);
    pktTotal = ntohl (pktTotal);
    checksum = ntohl (checksum);
    timestamp = ntohl (timestamp);
    duration = ntohl (duration);
  }
};

/// Standard data packet
struct packet {
  /// packet header
  packetHeader	header;
  /// packet payload
  char		payload[packetSize];
  /// swap byte-order in header from host to network
  inline void hton();
  /// swap byte-order in header from network to host
  inline void ntoh();
};

#endif // GDSBROADCASTTEST_GDS_PACKET_H
