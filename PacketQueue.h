//
// Created by erik.vonreis on 3/7/23.
//

#ifndef GDS_BCST_TEST_PACKETQUEEU_H
#define GDS_BCST_TEST_PACKETQUEEU_H

#include <deque>
#include <condition_variable>
#include <mutex>
#include <chrono>

template <class T>
class PacketQueue {

  public:
    void Push(T item) {
      std::unique_lock<std::mutex> lock(mut);
      queue.push_back(item);
      pushed.notify_one();
    }

    // returns non-zero if successful
    // otherwise times out after 1 second and returns 0
    int Pop(T* item) {
      std::unique_lock<std::mutex> lock(mut);
      if (queue.empty()) {
        std::chrono::seconds wait(1);
        auto status = pushed.wait_for(lock, wait,
                                      [this](){return !this->queue.empty();
                                      });
        if (!status) {
          return 0;
        }
      }
      *item = queue[0];
      queue.pop_front();
      return 1;
    }

    int size() {
      return queue.size();
    }

  private:
    std::deque<T> queue;
    std::condition_variable pushed;
    mutable std::mutex mut;

};

#endif // GDS_BCST_TEST_PACKETQUEEU_H
