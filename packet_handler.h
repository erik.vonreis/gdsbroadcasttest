//
// Created by erik.vonreis on 3/7/23.
//

#ifndef GDS_BCST_TEST_PACKET_HANDLER_H
#define GDS_BCST_TEST_PACKET_HANDLER_H

namespace receive {

  void handle_packets(PacketQueue<char *> *queue,
                    PacketQueue<char *> *return_queue, int *quit);
}

#endif // GDS_BCST_TEST_PACKET_HANDLER_H
