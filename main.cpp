//
// Created by erik.vonreis on 3/7/23.
//
#include <stdio.h>
#include <stdlib.h>

#include "receive.hh"
#include "transmit.h"

const int COMMAND_RECEIVE=1;

void usage()
{
  fprintf(stderr, "gds_bcst r <port>\n"
               "\treceive gds broadcast packets and report some statistics\n");
  fprintf(stderr, "gds_bcst t <addr> <port> <rate_kbs>\n"
                  "\treceive gds broadcast packets and report some statistics\n");
}


int run_receive(int argc, char *argv[])
{
  if(argc<1) {
    fprintf(stderr, "Error: missing port number\n\n");
    usage();
    return 1;
  }

  char *port_str = argv[0];

  int port = atoi(port_str);

  receive::receive(port);

  return 0;
}

int run_transmit(int argc, char *argv[]) {
  if(argc < 3) {
    fprintf(stderr, "Error: not enough arugment for transmit\n\n");
    usage();
    return 1;
  }

  char *address = argv[0];
  int port = atoi(argv[1]);
  int rate_kb = atoi(argv[2]);

  transmit::transmit(address, port, rate_kb);

  return 0;
}



int main(int argc, char *argv[])
{
  if(argc < 2) {
    fprintf(stderr, "%s", "Error: no command given\n\n");
    usage();
    return 1;
  }

  char *cmd = argv[1];

  switch(cmd[0]) {
  case 'r':
    return run_receive(argc-2, argv+2);
  case 't':
    return run_transmit(argc-2, argv+2);
  default:
    fprintf(stderr, "Error: unrecognized command '%s'\n\n", cmd);
    usage();
    return 1;
  }
}
