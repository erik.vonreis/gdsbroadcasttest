//
// Created by erik.vonreis on 3/7/23.
//
#include <stdio.h>
#include <sys/time.h>
#include <vector>
#include <deque>
#include "memory.h"
#include "PacketQueue.h"
#include "gds_packet.h"




namespace receive {

  class SequenceTracker {
    std::vector<int> received;

    uint32_t num_missing;

  public:
    uint32_t sequence_number;
    SequenceTracker(uint32_t seq, uint32_t num_pkts): received((size_t)num_pkts), sequence_number{seq}, num_missing(num_pkts) {

    }

    void receive(uint32_t i) {
      if(i >= 0 && i < received.size()) {
        if (!received[i]) {
          --num_missing;
          received[i] = 1;
        }

      }
      //printf("received_size=%d missing=%d\n", received.size(), num_missing);
    }

    int missing() {
      return num_missing;
    }
  };


  std::deque<SequenceTracker> sequences;

  /**
   *
   * @param header
   * @return number of missing packets discovered on this check.
   */
  int sequence_check(packetHeader *header) {
    const int max_sequences = 5;
    static int first = 1;
    int total_missing = 0;

    if(sequences.empty()) {
      //printf("adding new sequence\n");
      sequences.emplace_back(header->seq, header->pktTotal);
    }

    //update the sequence numbered in the packet.
    uint32_t base_sequence = sequences[0].sequence_number;
    auto index = header->seq - base_sequence;
    //printf("seq=%d index=%d\n", header->seq, index);
    if (index < sequences.size() ){
      sequences[index].receive(header->pktNum);
    }
    else if(index == sequences.size()) {
      //printf("adding new sequence\n");
      sequences.emplace_back(header->seq, header->pktTotal);
      sequences[index].receive(header->pktNum);
    }
    else {
      fprintf(stderr, "bad sequence index %d when checking sequences\n",
          index );
      index = 0;
      sequences.clear();
      first = 1;
      sequences.emplace_back(header->seq, header->pktTotal);
      sequences[index].receive(header->pktNum);
    }

    // if too many packets are being tracked,
    // abandon the earliest one
    if (sequences.size() > max_sequences) {
      uint32_t seq = sequences[0].sequence_number;
      if(!first) {
        total_missing += sequences[0].missing();
      }
      else {
        first = 0;
      }
      sequences.pop_front();
      //printf("dropping sequence %d\n", seq);
    }

    while(sequences.size() > 0 && sequences[0].missing() == 0) {
      first = 0;
      uint32_t seq = sequences[0].sequence_number;
      sequences.pop_front();
      //printf("sequence %d complete\n", seq);
    }

    return total_missing;
  }

  struct RXStat {
    int packet_count;
    int missed_packets;
    long long total_bytes;
    int recv_queue_sum;
    int return_queue_sum;
    int queue_count;
    timeval start_time;
    timeval end_time;
    char title[64];


    RXStat(const char *name) {
      gettimeofday(&end_time, NULL);
      clear();
      strncpy(title, name, sizeof(title));
    }

    /**
     * End the collection period
     */
    void end() {
      gettimeofday(&end_time, NULL);
      periodic_report();
      clear();
    }

    /**
     * Clears out the stats and starts a new collection period
     * coincident with the end of the last period
     */
    void clear() {
      packet_count = 0;
      missed_packets = 0;
      total_bytes = 0;
      recv_queue_sum = 0;
      return_queue_sum = 0;
      queue_count = 0;
      start_time = end_time;
    }

    void update_packet(int new_missed_packets, int new_bytes ) {
      ++packet_count;
      missed_packets += new_missed_packets;
      total_bytes += new_bytes;
    }

    void update_queue(int recv_queue_size,
                      int return_queue_size) {
      recv_queue_sum += recv_queue_size;
      return_queue_sum += return_queue_size;
      ++queue_count;
    }

    void periodic_report() {
      timeval delta;
      timersub(&end_time, &start_time, &delta);
      double delta_s = delta.tv_sec + (delta.tv_usec / 1e6);

      if (queue_count == 0) {
        queue_count = 1;
      }

      if (delta_s > 0) {
        printf("%s\tDT=%0.1lf PKTS=%0.1lf Dropped=%0.1lf kb/s=%0.1lf Q=%0.1lf, rQ=%0.1lf\n",
               title, delta_s, packet_count / delta_s, missed_packets / delta_s,
               total_bytes / (1024 * delta_s), recv_queue_sum / (double)queue_count, return_queue_sum / (double)queue_count);
      }
      else {
        fprintf(stderr, "Delta time for stat report %s was %lf, but must be positive.\n",
            title, delta_s);
      }
    }

    void sum_report() {


      printf("%s\tPKTS=%d Dropped=%d kb=%lld\n",
             title,  packet_count, missed_packets, total_bytes/1024  );

    }

    time_t sec_since_start(timeval *now) {
      timeval delta;
      timersub(now, &start_time, &delta);
      return delta.tv_sec;
    }

  };

  static RXStat sec_stat("SEC");
  static RXStat min_stat("MIN");
  static RXStat total_stat("TOT");

  void process_packet(packet *pkt) {
    static const char *type_names[] = {"BCST", "REBCST", "REXMT"};
    pkt->header.ntoh();
    packetHeader *header = &pkt->header;
    int type_index = header->pktType - PKT_BROADCAST;
    const char * type_name = "UNKNWN";
    if ((type_index<= 2) && (type_index >= 0)) {
      type_name = type_names[type_index];
    }
//    printf("pkt pktType=%s seq=%d pktNum=%d pktTotal=%d pktLen=%d\n",
//           type_name,
//           header->seq, header->pktNum, header->pktTotal, header->pktLen);


    if (header->pktType == PKT_BROADCAST) {
      int missed_packets = 0;

      // check sequence
      missed_packets += sequence_check(header);


      // update stats
      sec_stat.update_packet(missed_packets,
                             sizeof(packetHeader) + header->pktLen);
      min_stat.update_packet(missed_packets,
                             sizeof(packetHeader) + header->pktLen);
      total_stat.update_packet(missed_packets,
                             sizeof(packetHeader) + header->pktLen);

    }
    else {
      //ignore other types
    }



  }


  /**
       * Read incoming packets from a queue and deal with them.
   */
  void handle_packets(PacketQueue<char *> *queue,
                      PacketQueue<char *> *return_queue, int *quit) {
    printf("Starting handle packet thread\n");
    char *buff;
    while (!*quit) {
      if (queue->Pop(&buff)) {
        process_packet((packet *)buff);
        return_queue->Push(buff);
        if (return_queue->size() < 4) {
          return_queue->Push(new char[1 << 16]);
        }
      } else {

      }

      // update queue stats
      int qsize = queue->size();
      int rqsize = return_queue->size();
      sec_stat.update_queue(qsize, rqsize);
      min_stat.update_queue(qsize, rqsize);
      total_stat.update_queue(qsize, rqsize);

      // handle stat reports
      timeval now;
      gettimeofday(&now, NULL);
      if (sec_stat.sec_since_start(&now) >= 5) {
        sec_stat.end();
      }
      if (min_stat.sec_since_start(&now) >= 60) {
        min_stat.end();
        total_stat.sum_report();
      }
    }
    printf("ending packet handler thread\n");
  }
}