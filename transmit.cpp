//
// Created by erik.vonreis on 3/8/23.
//
#include <stdio.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <sys/time.h>
#include <errno.h>
#include <math.h>
#include <fcntl.h>

#include "gds_packet.h"

packet pkt;




namespace transmit {
  void transmit(const char *address, int port, int rate_kb) {

    // open socket
    int sock = socket(AF_INET, SOCK_DGRAM, 0);
    if (sock < 0) {
      fprintf(stderr, "couldn't open socket\n");
      return;
    }

    int flags = fcntl(sock, F_GETFL, 0);
    flags = flags | O_NONBLOCK;
    if( fcntl(sock, F_SETFL, flags) )
    {
      fprintf(stderr, "Couldn't set non-blocking on socket\n");
    }

    int bcast = 1;
    if(setsockopt(sock, SOL_SOCKET, SO_BROADCAST, &bcast, sizeof(bcast))) {
      fprintf(stderr, "failed to set broadcast option\n");
    }

    sockaddr_in send_addr;
    if(!inet_pton(AF_INET, address, &(send_addr.sin_addr))) {
      fprintf(stderr, "'%s' was not a valid internet address\n", address);
      close(sock);
      return;
    }
    send_addr.sin_port = htons(port);
    send_addr.sin_family = AF_INET;

    int pkt_size_bytes = 64000;

    int pkts_per_sec = (rate_kb * 1024) / pkt_size_bytes;
    if (pkts_per_sec < 1) {
      pkts_per_sec = 1;
    }


    int desired_dt_usec = (int)(1e6 / pkts_per_sec);
    int sleep_time_usec = desired_dt_usec - 50;

    if(sleep_time_usec < 1) {
      sleep_time_usec = 1;
    }

    printf("packet size = %d bytes\n", pkt_size_bytes);
    printf("period = %d us\n", desired_dt_usec );

    const int pktTotal = 12;
    int seq = 0;
    int pktNum = 0;

    pkt.header.pktLen=pkt_size_bytes;
    pkt.header.pktTotal=pktTotal;

    pkt.header.pktType = PKT_BROADCAST;

    pkt.header.hton();

    long total_bytes = 0;
    timeval last_time;
    timeval now, delta;

    gettimeofday(&last_time, NULL);

    int count = 0;

    while(1) {

      pkt.header.seq = htonl(seq);
      pkt.header.pktNum = htonl(pktNum);

      ++pktNum;
      if(pktNum >= pktTotal) {
        ++seq;
        pktNum = 0;
      }

      int n = 0;

      // comment this in to produce one dropped packet per seocond.
      //if(count)
      n = sendto(sock, &pkt, sizeof(packetHeader) + pkt_size_bytes, 0,
                     (sockaddr *)&send_addr, sizeof(send_addr));
      if (n < 1) {
        if (errno = EWOULDBLOCK) {
          total_bytes += sizeof(packetHeader) + pkt_size_bytes;
        } else {
          fprintf(stderr, "sendto error %d\n", errno);
        }
      } else {
        total_bytes += n;
      }

      ++count;

      if (count >= pkts_per_sec)
      {

        gettimeofday(&now, NULL);
        timersub(&now, &last_time, &delta);
        if (delta.tv_sec > 5) {
          double delta_s = delta.tv_sec;
          delta_s += delta.tv_usec / 1e6;


          double actual_rate_kbs = total_bytes / (1024.0 * delta_s);
          double actual_dt_usec = 1e6*pkt_size_bytes / (1024*actual_rate_kbs);
          double delta_sleep_usec = desired_dt_usec - actual_dt_usec;
          sleep_time_usec += (int)(delta_sleep_usec + 0.5);
          if(sleep_time_usec < 1) {
            sleep_time_usec = 1;
          }
          printf("actual_dt = %0.1lf usec est=%0.1lf usec\n", actual_dt_usec, delta_sleep_usec);
          printf("xmit rate = %0.1lf kb/s sleep=%d usec\n", actual_rate_kbs, sleep_time_usec);
          total_bytes = 0;
          last_time = now;
        }
        count = 0;
      }
      usleep(sleep_time_usec);
    }


  }
}