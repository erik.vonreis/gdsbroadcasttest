//
// Created by erik.vonreis on 3/8/23.
//

#ifndef GDS_BCST_TEST_TRANSMIT_H
#define GDS_BCST_TEST_TRANSMIT_H

namespace transmit {
  void transmit(const char *address, int port, int rate_kb);
}

#endif // GDS_BCST_TEST_TRANSMIT_H
