//
// Created by erik.vonreis on 3/7/23.
//
#include <thread>
#include <stdio.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <memory.h>
#include <unistd.h>
#include <signal.h>

#include "PacketQueue.h"
#include "packet_handler.h"

namespace receive {



  char *get_buffer(PacketQueue<char *> *return_queue) {
    if(return_queue->size() > 0) {
      char *buff;
      if(return_queue->Pop(&buff)) {
        return buff;
      }
    }
    return new char[1<<16];
  }



  void receive_thread(int port,
                      PacketQueue<char *> *queue,
                      PacketQueue<char *> *return_queue,
                      int *quit)
  {
    printf("In receive thread\n");

    // open socket
    int sock = socket(AF_INET, SOCK_DGRAM, 0);
    if(sock < 0) {
      fprintf(stderr, "socket creation failed");
      return;
    }

    // try to set recv buff size as big as possible

    int increment = 1<<17;
    int sock_buf_size = increment;
    socklen_t optlen = sizeof(sock_buf_size);
    getsockopt(sock, SOL_SOCKET,SO_RCVBUF, &sock_buf_size, &optlen);
    printf("rxbufsize=%d optlen=%d\n", sock_buf_size, optlen);
    while(sock_buf_size < 1<<20) {
      if( setsockopt(sock, SOL_SOCKET, SO_RCVBUF,
                     &sock_buf_size, sizeof(sock_buf_size))) {
        printf("Could not set rx buffer size to %d\n", sock_buf_size);
        break;
      }
      printf("Rx buf size %d works\n", sock_buf_size);
      sock_buf_size += increment;
    }
    sock_buf_size -= increment;
    printf("Using sock buf size of %d\n", sock_buf_size);

    // set timeout
    timeval timeout = {1,0};
    if( setsockopt(sock, SOL_SOCKET, SO_RCVTIMEO, &timeout, sizeof(timeout))) {
      fprintf(stderr, "Couldn't set timeout on socket");
    }

    sockaddr_in local_addr, remote_addr;
    memset(&local_addr, 0, sizeof(local_addr));
    memset(&remote_addr, 0, sizeof(local_addr));

    local_addr.sin_family = AF_INET;
    local_addr.sin_addr.s_addr = INADDR_ANY;
    local_addr.sin_port = htons(port);

    if( bind(sock, (sockaddr *)&local_addr, sizeof(local_addr)) < 0)
    {
      fprintf(stderr, "socket bind failed");
      close(sock);
      return;
    }

    socklen_t len;
    len = sizeof(remote_addr);

    char *buffer = get_buffer(return_queue);

    while( ! *quit ) {
      int n = recvfrom(sock, (void *)buffer, 1 << 16, 0, (sockaddr *)&remote_addr,
                       &len);

      if (n > 0) {
        //printf("got %d bytes from 0x%x\n", n, ntohl(remote_addr.sin_addr.s_addr));

        queue->Push(buffer);
        buffer = get_buffer(return_queue);
      } else {
        if( (errno == EAGAIN) || (errno == EWOULDBLOCK)) {
          //printf("No packet read, timeout\n");
        }
        else {
          //fprintf(stderr, "Receive failed with errno %d", errno);
          break;
        }
      }
    }
    printf("closing socket\n");
    printf("ending receive thread\n");
    close(sock);

  }

  int quit=0;

  void int_handler(int dummy) {
    quit = 1;
  }

  int receive(int port) {

    printf("Receiving GDS packets on port %d\n", port);

    // setup ctrl-c handler
    signal(SIGINT, &int_handler);

    // create queue
    PacketQueue<char *> queue;

    // packet buffers are returned ot the receiver here so we don't have to allocate them.
    PacketQueue<char *> return_queue;

    // create handler thread
    std::thread handler(&handle_packets, &queue, &return_queue, &quit);

    // create receiver thread
    std::thread rec_thread(&receive_thread, port, &queue, &return_queue, &quit);

    // done
    handler.join();
    rec_thread.join();

    return 0;
  }

}